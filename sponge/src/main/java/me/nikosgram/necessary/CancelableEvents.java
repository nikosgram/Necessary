/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import com.google.common.base.Optional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import me.nikosgram.necessary.api.EntityCategory;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.monster.Wither;
import org.spongepowered.api.entity.player.Player;
import org.spongepowered.api.entity.projectile.Projectile;
import org.spongepowered.api.entity.weather.Lightning;
import org.spongepowered.api.event.Subscribe;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.entity.EntitySpawnEvent;
import org.spongepowered.api.event.entity.player.PlayerChangeHealthEvent;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class CancelableEvents {
    private final NecessarySponge necessary;

    @Subscribe
    public void onPassiveMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Passive)) {
            if (necessary.config.getNode("disable", "spawn", "passive-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.BAT)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "bat").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.CHICKEN)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "chicken").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.COW)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "cow").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.MUSHROOM_COW)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "mooshroom").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.PIG)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "pig").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.RABBIT)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "rabbit").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SHEEP)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "sheep").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SQUID)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "squid").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.VILLAGER)) {
                if (necessary.config.getNode("disable", "spawn", "passive-mobs", "villager").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onNeutralMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Neutral)) {
            if (necessary.config.getNode("disable", "spawn", "neutral-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.CAVE_SPIDER)) {
                if (necessary.config.getNode("disable", "spawn", "neutral-mobs", "cave-spider").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.ENDERMAN)) {
                if (necessary.config.getNode("disable", "spawn", "neutral-mobs", "enderman").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SPIDER)) {
                if (necessary.config.getNode("disable", "spawn", "neutral-mobs", "spider").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.PIG_ZOMBIE)) {
                if (necessary.config.getNode("disable", "spawn", "neutral-mobs", "pig-zombie").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onHostileMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Hostile)) {
            if (necessary.config.getNode("disable", "spawn", "hostile-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.BLAZE)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "blaze").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.CREEPER)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "creeper").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.GUARDIAN)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "guardian").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.ENDERMITE)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "endermite").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.GHAST)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "ghast").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.MAGMA_CUBE)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "magma-cube").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SILVERFISH)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "silverfish").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SKELETON)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "skeleton").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SLIME)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "slime").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.WITCH)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "witch").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.WITHER_SKULL)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "wither-skull").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.ZOMBIE)) {
                if (necessary.config.getNode("disable", "spawn", "hostile-mobs", "zombie").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onTamableMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Tamable)) {
            if (necessary.config.getNode("disable", "spawn", "tamable-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.HORSE)) {
                if (necessary.config.getNode("disable", "spawn", "tamable-mobs", "horse").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onUtilityMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Utility)) {
            if (necessary.config.getNode("disable", "spawn", "utility-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.IRON_GOLEM)) {
                if (necessary.config.getNode("disable", "spawn", "utility-mobs", "iron-golem").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.SNOWMAN)) {
                if (necessary.config.getNode("disable", "spawn", "utility-mobs", "snowman").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onBossMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Boss)) {
            if (necessary.config.getNode("disable", "spawn", "boss-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.ENDER_DRAGON)) {
                if (necessary.config.getNode("disable", "spawn", "boss-mobs", "ender-dragon").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (event.getEntity().getType().equals(EntityTypes.WITHER)) {
                if (necessary.config.getNode("disable", "spawn", "boss-mobs", "wither").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onUnusedMobsSpawn(EntitySpawnEvent event) {
        if (EntityCategory.getCategory(event.getEntity().getType()).equals(EntityCategory.Unused)) {
            if (necessary.config.getNode("disable", "spawn", "unused-mobs").getBoolean()) {
                event.setCancelled(true);
            } else if (event.getEntity().getType().equals(EntityTypes.GIANT)) {
                if (necessary.config.getNode("disable", "spawn", "unused-mobs", "giant").getBoolean()) {
                    event.setCancelled(true);
                }
            }
        }
    }

    @Subscribe
    public void onLightningSpawn(EntitySpawnEvent event) {
        if (event.getEntity().getType().equals(EntityTypes.LIGHTNING)) {
            if (!necessary.config.getNode("weather", "lightning").getBoolean()) {
                event.setCancelled(true);
            }
        }
    }

    @Subscribe
    public void onPlayerChangeHealth(PlayerChangeHealthEvent event) {
        Optional<Cause> cause = event.getCause();
        if (cause.isPresent()) {
            if (cause.get().getCause() instanceof Player) {
                if (necessary.config.getNode("disable", "pvp").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (cause.get().getCause() instanceof Projectile) {
                if (necessary.config.getNode("disable", "projectiles-damage").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (cause.get().getCause() instanceof Wither) {
                if (necessary.config.getNode("disable", "wither-damage").getBoolean()) {
                    event.setCancelled(true);
                }
            } else if (cause.get().getCause() instanceof Lightning) {
                if (necessary.config.getNode("disable", "lightning-damage").getBoolean()) {
                    event.setCancelled(true);
                }
            }
            //TODO: waiting to add more causes. SpongeAPI Revision 3 or 2.5!
        }
    }
}