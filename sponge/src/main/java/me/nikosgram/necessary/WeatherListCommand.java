/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.command.CommandException;
import org.spongepowered.api.util.command.CommandResult;
import org.spongepowered.api.util.command.CommandSource;
import org.spongepowered.api.util.command.args.CommandContext;
import org.spongepowered.api.util.command.spec.CommandExecutor;
import org.spongepowered.api.world.weather.Weather;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class WeatherListCommand implements CommandExecutor {
    private final NecessarySponge necessary;

    @Override
    public CommandResult execute(CommandSource sender, CommandContext context) throws CommandException {
        List<Text> messages = new ArrayList<Text>();
        int page = context.hasAny("page") ? (context.<Integer>getOne("page").get()) : 1;
        messages.add(Texts.builder("Weathers List (Page ").color(TextColors.GRAY).append(
                Texts.builder(String.valueOf(page)).color(TextColors.RED).build()).append(
                Texts.builder(")").color(TextColors.GRAY).build()).build());
        List<Weather> weathers = (List<Weather>) necessary.game.getRegistry().getAllOf(Weather.class);
        for (Weather weather : NecessarySponge.page(page, 5, weathers)) {
            messages.add(Texts.builder(weather.getName()).color(TextColors.RED).build());
        }
        sender.sendMessage(messages);
        return CommandResult.success();
    }
}