/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.spongepowered.api.entity.player.Player;
import org.spongepowered.api.entity.player.gamemode.GameMode;
import org.spongepowered.api.text.Texts;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.command.CommandException;
import org.spongepowered.api.util.command.CommandPermissionException;
import org.spongepowered.api.util.command.CommandResult;
import org.spongepowered.api.util.command.CommandSource;
import org.spongepowered.api.util.command.args.CommandContext;
import org.spongepowered.api.util.command.spec.CommandExecutor;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class GamemodeCommand implements CommandExecutor {
    private final NecessarySponge necessary;

    @Override
    public CommandResult execute(CommandSource sender, CommandContext context) throws CommandException {
        Player player;
        if (!context.hasAny("player")) {
            if (sender instanceof Player) {
                player = (Player) sender;
            } else {
                throw new CommandException(Texts.builder("This command using only from players.").color(TextColors.RED).build());
            }
        } else if (!sender.hasPermission("necessary.gamemode.others")) {
            throw new CommandPermissionException();
        } else {
            player = context.<Player>getOne("player").get();
        }
        GameMode mode = context.<GameMode>getOne("mode").get();
        player.getGameModeData().setGameMode(mode);
        player.sendMessage(
                Texts.builder("Your gamemode has change to ").color(TextColors.GRAY).append(
                        Texts.builder(mode.getName()).color(TextColors.RED).build())
                        .append(Texts.builder(".").color(TextColors.GRAY).build()).build());
        for (Player notify : necessary.server.getOnlinePlayers()) {
            if (notify.hasPermission("necessary.gamemode.notify")) {
                if (!notify.getUniqueId().equals(player.getUniqueId())) {
                    notify.sendMessage(
                            Texts.builder("The gamemode change to ").color(TextColors.GRAY).append(
                                    Texts.builder(mode.getName()).color(TextColors.RED).build())
                                    .append(Texts.builder(" for ").color(TextColors.GRAY).build()).append(
                                    player.getDisplayNameData().getDisplayName())
                                    .append(Texts.builder(".").color(TextColors.GRAY).build()).build());
                }
            }
        }
        return CommandResult.success();
    }
}