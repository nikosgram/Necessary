/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.necessary.api;

import lombok.Getter;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;

public enum EntityCategory {
    Passive(EntityTypes.BAT, EntityTypes.CHICKEN, EntityTypes.COW, EntityTypes.MUSHROOM_COW, EntityTypes.RABBIT,
            EntityTypes.SHEEP, EntityTypes.SQUID, EntityTypes.VILLAGER),
    Neutral(EntityTypes.CAVE_SPIDER, EntityTypes.ENDERMAN, EntityTypes.SPIDER, EntityTypes.PIG_ZOMBIE),
    Hostile(EntityTypes.BLAZE, EntityTypes.CREEPER, EntityTypes.GUARDIAN, EntityTypes.ENDERMITE, EntityTypes.GHAST,
            EntityTypes.MAGMA_CUBE, EntityTypes.SILVERFISH, EntityTypes.SKELETON, EntityTypes.SLIME, EntityTypes.WITCH,
            EntityTypes.WITHER_SKULL, EntityTypes.ZOMBIE),
    Tamable(EntityTypes.HORSE),
    Utility(EntityTypes.IRON_GOLEM, EntityTypes.SNOWMAN),
    Boss(EntityTypes.ENDER_DRAGON, EntityTypes.WITHER),
    Unused(EntityTypes.GIANT),
    None();

    @Getter
    private final EntityType[] entities;

    EntityCategory(EntityType... entities) {
        this.entities = entities;
    }

    public static EntityCategory getCategory(EntityType type) {
        for (EntityCategory category : values()) {
            for (EntityType entity : category.getEntities()) {
                if (entity.equals(type)) return category;
            }
        }
        return None;
    }
}